package proyecto1.Parsers2;
import java_cup.runtime.Symbol;

%%

%cupsym tabla_simbolos2
%class Scanner
%cup
%public
%unicode
%line
%column
%char
%ignorecase

numeros=[0-9]+|[\-][0-9]+
letras=[a-zA-ZáéíóúÁÉÍÓÚÑñ0-9]
nombre= {letras}+[\_]*{letras}+
caracter=[\"][^\.]*[\"]


%%

/*PALABRAS RESERVADAS*/

"<escenario" {return new Symbol(tabla_simbolos2.tescenario, yycolumn,yyline, new String(yytext()));}
"</escenario>" {return new Symbol(tabla_simbolos2.tescenariocierre, yycolumn,yyline, new String(yytext()));}
"fondo=" {return new Symbol(tabla_simbolos2.tfondo, yycolumn,yyline, new String(yytext()));}
"ancho=" {return new Symbol(tabla_simbolos2.tancho, yycolumn,yyline, new String(yytext()));}
"alto=" {return new Symbol(tabla_simbolos2.talto, yycolumn,yyline, new String(yytext()));}
">" {return new Symbol(tabla_simbolos2.tmayor, yycolumn,yyline, new String(yytext()));}
"<paredes>" {return new Symbol(tabla_simbolos2.tparedes, yycolumn,yyline, new String(yytext()));}
"</paredes>" {return new Symbol(tabla_simbolos2.tparedescierre, yycolumn,yyline, new String(yytext()));}
"<extras>" {return new Symbol(tabla_simbolos2.textras, yycolumn,yyline, new String(yytext()));}
"</extras>" {return new Symbol(tabla_simbolos2.textrascierre, yycolumn,yyline, new String(yytext()));}
"<estrella>" {return new Symbol(tabla_simbolos2.testrella, yycolumn,yyline, new String(yytext()));}
"</estrella>" {return new Symbol(tabla_simbolos2.testrellacierre, yycolumn,yyline, new String(yytext()));}
"<bonus>" {return new Symbol(tabla_simbolos2.tbonus, yycolumn,yyline, new String(yytext()));}
"</bonus>" {return new Symbol(tabla_simbolos2.tbonuscierre, yycolumn,yyline, new String(yytext()));}
"<manzana>" {return new Symbol(tabla_simbolos2.tmanzana, yycolumn,yyline, new String(yytext()));}
"</manzana>" {return new Symbol(tabla_simbolos2.tmanzanacierre, yycolumn,yyline, new String(yytext()));}
"<veneno>" {return new Symbol(tabla_simbolos2.tveneno, yycolumn,yyline, new String(yytext()));}
"</veneno>" {return new Symbol(tabla_simbolos2.tvenenocierre, yycolumn,yyline, new String(yytext()));}
"<bomba>" {return new Symbol(tabla_simbolos2.tbomba, yycolumn,yyline, new String(yytext()));}
"</bomba>" {return new Symbol(tabla_simbolos2.tbombacierre, yycolumn,yyline, new String(yytext()));}
 
"(" {return new Symbol(tabla_simbolos2.tparentesisa, yycolumn,yyline, new String(yytext()));}
")" {return new Symbol(tabla_simbolos2.tparentesisc, yycolumn,yyline, new String(yytext()));}
"," {return new Symbol(tabla_simbolos2.tcoma, yycolumn,yyline, new String(yytext()));}
"." {return new Symbol(tabla_simbolos2.tpunto, yycolumn,yyline, new String(yytext()));}
"=" {return new Symbol(tabla_simbolos2.tigual, yycolumn,yyline, new String(yytext()));}
";" {return new Symbol(tabla_simbolos2.tpuntoycoma, yycolumn,yyline, new String(yytext()));}


{numeros} {return new Symbol(tabla_simbolos2.ernumbers, yycolumn,yyline,new String(yytext()));}

{letras} {return new Symbol(tabla_simbolos2.erletters, yycolumn,yyline,new String(yytext()));}

{nombre} {return new Symbol(tabla_simbolos2.ername, yycolumn,yyline,new String(yytext()));}

{caracter} {return new Symbol(tabla_simbolos2.caracter, yycolumn,yyline,new String(yytext()));}



/* ESPACIOS EN BLANCO */
[ \t\r\f\n]+ { /* Se ignoran */}

/* CUALQUIER OTRO */
. { return new Symbol(tabla_simbolos2.terrorlex, yycolumn,yyline, new String(yytext())); }