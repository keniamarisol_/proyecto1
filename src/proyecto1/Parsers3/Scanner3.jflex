package proyecto1.Parsers3;
import java_cup.runtime.Symbol;

%%

%cupsym tabla_simbolos1
%class Scanner
%cup
%public
%unicode
%line
%column
%char
%ignorecase

letras=[a-zA-ZáéíóúÁÉÍÓÚÑñ0-9]
nombre= {letras}+[\_]*{letras}+
caracter=[\"][^\.]*[\"]


%%

/*PALABRAS RESERVADAS*/


"estrategias" {return new Symbol(tabla_simbolos.testrategias, yycolumn,yyline, new String(yytext())); }
"Movimientos" {return new Symbol(tabla_simbolos.tmovimientos, yycolumn,yyline, new String(yytext())); }
"arriba" {return new Symbol(tabla_simbolos.tarriba, yycolumn,yyline, new String(yytext())); }
"abajo" {return new Symbol(tabla_simbolos.tabajo, yycolumn,yyline, new String(yytext())); }
"derecha" {return new Symbol(tabla_simbolos.tderecha, yycolumn,yyline, new String(yytext())); }
"izquierda" {return new Symbol(tabla_simbolos.tizquierda, yycolumn,yyline, new String(yytext())); }

"{" {return new Symbol(tabla_simbolos.tllave_abierta, yycolumn,yyline, new String(yytext())); }
"}" {return new Symbol(tabla_simbolos.tllave_cerrada, yycolumn,yyline, new String(yytext())); }
":" {return new Symbol(tabla_simbolos.tdos_puntos, yycolumn,yyline, new String(yytext())); }
"[" {return new Symbol(tabla_simbolos.tcorchete_abierto, yycolumn,yyline, new String(yytext())); }
"]" {return new Symbol(tabla_simbolos.tcorchete_cerrado, yycolumn,yyline, new String(yytext())); }
"," {return new Symbol(tabla_simbolos.tcoma_opcional, yycolumn,yyline, new String(yytext())); }


/*LETRAS*/
{letras} {return new Symbol(tabla_simbolos1.erletters, yycolumn,yyline,new String(yytext()));}

{nombre} {return new Symbol(tabla_simbolos1.ername, yycolumn,yyline,new String(yytext()));}

{caracter} {return new Symbol(tabla_simbolos1.caracter, yycolumn,yyline,new String(yytext()));}



/* ESPACIOS EN BLANCO */
[ \t\r\f\n]+ { /* Se ignoran */}

/* CUALQUIER OTRO */
. { return new Symbol(tabla_simbolos1.terrorlex, yycolumn,yyline, new String(yytext())); }