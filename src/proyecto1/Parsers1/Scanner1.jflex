package proyecto1.Parsers1;
import java_cup.runtime.Symbol;

%%

%cupsym tabla_simbolos1
%class Scanner
%cup
%public
%unicode
%line
%column
%char
%ignorecase

numeros=[0-9]+|[\-][0-9]+
letras=[a-zA-ZáéíóúÁÉÍÓÚÑñ0-9]
nombre= {letras}+[\_]*{letras}+
/*vida= [1-9][0]|([1][0][0])*/
nombreimagen = {nombre}[\.]{letras}
directorio= [\"]([\/]{letras}+)+[\.][a-zA-Z]+[\"]
caracter=[\"][^\.]*[\"]


%%

/*PALABRAS RESERVADAS*/


"<configuracion>" {return new Symbol(tabla_simbolos1.tconfiguration, yycolumn,yyline, new String(yytext()));}
"</configuracion>" {return new Symbol(tabla_simbolos1.tconfigurationcierre, yycolumn,yyline, new String(yytext()));}

"<fondo>" {return new Symbol(tabla_simbolos1.tbackground, yycolumn,yyline, new String(yytext()));}
"</fondo>" {return new Symbol(tabla_simbolos1.tbackgroundcierre, yycolumn,yyline, new String(yytext()));}

"<disenio>" {return new Symbol(tabla_simbolos1.tdesign, yycolumn,yyline, new String(yytext()));}
"</disenio>" {return new Symbol(tabla_simbolos1.tdesigncierre, yycolumn,yyline, new String(yytext()));}

"nombre" {return new Symbol(tabla_simbolos1.tname, yycolumn,yyline, new String(yytext()));}
"path" {return new Symbol(tabla_simbolos1.tpath, yycolumn,yyline, new String(yytext()));}
"tipo" {return new Symbol(tabla_simbolos1.ttype, yycolumn,yyline, new String(yytext()));}
"manzana" {return new Symbol(tabla_simbolos1.tapple, yycolumn,yyline, new String(yytext()));}
"bloque" {return new Symbol(tabla_simbolos1.tblock, yycolumn,yyline, new String(yytext()));}
"bonus" {return new Symbol(tabla_simbolos1.tbonus, yycolumn,yyline, new String(yytext()));}
"bomba" {return new Symbol(tabla_simbolos1.tbomb, yycolumn,yyline, new String(yytext()));}
"estrella" {return new Symbol(tabla_simbolos1.tstar, yycolumn,yyline, new String(yytext()));}
"veneno" {return new Symbol(tabla_simbolos1.tvenom, yycolumn,yyline, new String(yytext()));}
"puntos" {return new Symbol(tabla_simbolos1.tpoints, yycolumn,yyline, new String(yytext()));}
"crecimiento" {return new Symbol(tabla_simbolos1.tincrease, yycolumn,yyline, new String(yytext()));}
"tiempo" {return new Symbol(tabla_simbolos1.ttime, yycolumn,yyline, new String(yytext()));}

 
"[" {return new Symbol(tabla_simbolos1.tcorchetea, yycolumn,yyline, new String(yytext()));}
"]" {return new Symbol(tabla_simbolos1.tcorchetec, yycolumn,yyline, new String(yytext()));}
"," {return new Symbol(tabla_simbolos1.tcoma, yycolumn,yyline, new String(yytext()));}
"=" {return new Symbol(tabla_simbolos1.tigual, yycolumn,yyline, new String(yytext()));}
";" {return new Symbol(tabla_simbolos1.tpuntoycoma, yycolumn,yyline, new String(yytext()));}



/*NUMEROS*/
{numeros} {return new Symbol(tabla_simbolos1.ernumbers, yycolumn,yyline,new String(yytext()));}

/*LETRAS*/
{letras} {return new Symbol(tabla_simbolos1.erletters, yycolumn,yyline,new String(yytext()));}

{nombre} {return new Symbol(tabla_simbolos1.ername, yycolumn,yyline,new String(yytext()));}

{nombreimagen} {return new Symbol(tabla_simbolos1.ernombreimagen, yycolumn,yyline,new String(yytext()));}

{directorio} {return new Symbol(tabla_simbolos1.erdirectory, yycolumn,yyline,new String(yytext()));}

{caracter} {return new Symbol(tabla_simbolos1.caracter, yycolumn,yyline,new String(yytext()));}



/* ESPACIOS EN BLANCO */
[ \t\r\f\n]+ { /* Se ignoran */}

/* CUALQUIER OTRO */
. { return new Symbol(tabla_simbolos1.terrorlex, yycolumn,yyline, new String(yytext())); }